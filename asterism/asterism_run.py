#!/usr/bin/env python

from __future__ import division, absolute_import, print_function

from asterism.analysis_pipelines.morphometric_features_extraction_euclid import  MorphometricFeatureExtractionPipeline
import glob
import argparse
import os
import numpy as np

def run(infile_name,conf_file,input_seg_map=None):


    base_name=os.path.basename(infile_name)

    #assume you want to have file where you write
    #number of source as a function of h_frac
    #out_file=open('%s_debl_res.dat'%base_name,'w')
    #print ("#h_frac_min h_frac_max   eps_frac  n_srcs    n_srcs_debl",file=out_file)

    #we set a grid of h_frac_values

    pipeline=MorphometricFeatureExtractionPipeline(parser=None,argv=None,conf_file=conf_file)


    pipeline.set_pars_from_conf_file()

    #sets the input file
    pipeline.IO_conf_task.set_par('infile',value=infile_name)
    pipeline.IO_conf_task.set_par('input_seg_map_file',value=input_seg_map)

    #sets the deblending method
    #allowed methods= 'guass_laplace_watershed', 'denclue' ,'denclue_watershed_deblending','no_deblending'
    #pipeline.do_src_detection.set_deblending_method.set_par('method',value='denclue')

    flag='set_the_output_flag'
    pipeline.IO_conf_task.set_par('flag',value=flag)
    products_collection=pipeline.run()
    

def main(argv=None):
    parser = argparse.ArgumentParser()


    parser.add_argument('input_files', type=str, default=None)
    parser.add_argument('conf_file', type=str, default=None)
    parser.add_argument('-input_seg_map', type=str, default=None)
    args = parser.parse_args()

    in_files_list=glob.glob(args.input_files)
    print (in_files_list)
    if in_files_list==[]:
        raise RuntimeError('No input files found')

    for file_name in in_files_list:
        run(file_name,args.conf_file,input_seg_map=args.input_seg_map)




if  __name__ == "__main__":
    main()
